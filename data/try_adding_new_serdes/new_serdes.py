"""Serializer-Deserializer [SERDES] for NEW"""
# STANDARD IMPORTS
try:
    import cPickle as pickle
except:
    import pickle


# LOCAL IMPORTS
from serdes_interface import SerdesInterface


class NewSerdes(SerdesInterface):
    """

    """
    def __init__(self):
        super(NewSerdes, self).__init__()

    def read(self, data):
        return pickle.loads(data)

    def write(self,  data):
        return pickle.dumps(data)

