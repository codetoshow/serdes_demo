1. Copy the "new_serdes.py" module to the plugin directory (src/serdes)

2. Add the following under "serdes:" in the configuration file (etc/serdes_demo_configuration.yml)
- class: NewSerdes
  module: new_serdes
  format: new


Note: "new_serdes.py" is a Pickle SERDES

