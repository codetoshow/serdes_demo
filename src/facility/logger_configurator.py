"""LoggerConfigurator and HandlerFormatter

HandlerFormatter OVERRIDES logging.Formatter.format() to allow separate formats for each log-level.

Example Usage:
    from logger_configuration import LoggerConfigurator

    LOG = logging.getLogger()
    ARGS = <args from argparse.ArgumentParser.parse_args()>
    log_configurator = LoggerConfigurator(LOG, ARGS)
"""
# STANDARD IMPORTS
import functools
import logging
import os

ROOT_LOGGER_NAME = ""


class SerdesDemoGetLogger(object):
    """
    A utility class that is used to maintain hierarchical loggers across modules

    Usage:
        from logger_configurator import SerdesDemoGetLogger
        LOG = SerdesDemoGetLogger().get_logger(__name__)
    """
    @staticmethod
    def get_logger(module_name):
        """
        Adjusts logger names for hierarchical loggers across packages.

        Note that the default config for loggers is to have their levels set to NOTSET

        :param module_name: __name__, as resolved in the calling module
        :return: a logger, "dot-hierarchically"-attached to the unwrap root logger
        """
        global ROOT_LOGGER_NAME
        module_basename = module_name.split(".")[-1]
        return logging.getLogger(ROOT_LOGGER_NAME + "." + module_basename)


class LoggerConfigurator(object):
    """
    Configures "LOG" (passed to constructor) with handlers/formatters
    """
    def __init__(self, LOG, skip_errors=False, debug=False, quiet=False):
        global ROOT_LOGGER_NAME

        ROOT_LOGGER_NAME = LOG.name
        """Holds the logger name.  Used by SerdesDe oGetLogger, which is used by all modules to
        get a logger that propagates its log-records to the root logger"""

        self._root_logger = LOG

        self._log_pathfile = os.path.join(os.environ["DATA_ROOT"],
                                          "serdes_demo_log.txt",
                                         )
        """ Fully-qualified name of the log-file"""

        # Control flags (cmd-line controllable)
        self._skip_errors = skip_errors
        self._debug = debug
        self._quiet = quiet

        crit_level_format = "%(levelname)s- %(message)s"
        errr_level_format = "%(levelname)s- %(message)s"
        warn_level_format = "%(levelname)s- %(message)s"
        xxxx_level_format = "%(levelname)s- %(message)s"
        debg_level_format = "%(asctime)s -%(levelname)s- %(name)s:%(funcName)s " \
                            "@%(lineno)d: %(message)s"
        self._log_format_data = {logging.CRITICAL: crit_level_format,
                                 logging.ERROR: errr_level_format,
                                 logging.warn: warn_level_format,
                                 logging.DEBUG: debg_level_format,
                                 logging.INFO: xxxx_level_format,
                                }

        self._log_level_data = dict(log_level_stream=0,
                                    log_level_file=logging.NOTSET,
                                   )

    def _get_stream_log_level(self):
        """
        The log's stream-handler is configurable from the command line using the
        "quiet" or "debug" flags/switches

        switch         desired effect
        ------         --------------
        --quiet        Not even Info .... so set to logging.warnING
        --debug        Debug

        quiet   debug   switch-results
        -----   -----   --------------
          F       F     INFO:INFO              INFO
          F       T     INFO:DEBUG             DEBUG
          T       F     warn:INFO              warn
          T       T     warn:DEBUG             NOT ALLOWED (argparse is setup to not allow this)

        :return: logging level
        """
        if self._quiet:
            return logging.WARNING
        elif self._debug:
            return logging.DEBUG

        return logging.INFO

    def _setup_file_handler(self):
        """ CREATE/CONFIGURE/ADD File HANDLER"""
        # CREATE
        file_handler = logging.FileHandler(filename=self._log_pathfile)
        # CONFIGURE
        file_handler.setFormatter(HandlerFormatter(self._log_format_data))
        file_handler.setLevel(self._log_level_data["log_level_file"])
        # ADD
        self._root_logger.addHandler(file_handler)

        msg = "Set FILE log level to {}".format(self._log_level_data["log_level_file"])
        self._root_logger.debug(msg)
        self._root_logger.info("LOG FILE : {}\n".format(self._log_pathfile))

    def _setup_stream_handler(self):
        """ CREATE/CONFIGURE/ADD Stream HANDLER"""
        # CREATE
        stream_handler = logging.StreamHandler()
        # CONFIGURE
        stream_handler.setFormatter(HandlerFormatter(self._log_format_data))

        stream_level = self._get_stream_log_level()
        stream_handler.setLevel(stream_level)
        # ADD
        self._root_logger.addHandler(stream_handler)

        msg = "Set STREAM log level to {}".format(stream_level)
        self._root_logger.debug(msg)

    def configure_logger(self):
        """
        Configure a logger that passes ALL log-records to the handlers.  The
        level is throttled by the handlers
        """
        # Ensure root is NOTSET
        self._root_logger.setLevel(logging.NOTSET)
        self._root_logger.propagate = False

        self._root_logger.handlers = []
        for handler in self._root_logger.handlers:
            print handler, handler.name, handler.level

        self._setup_stream_handler()
        self._setup_file_handler()


class HandlerFormatter(logging.Formatter):
    """
    OVERRIDES logging.Formatter.format() to allow formatting based on log-level

    Use data in "log_level_formats" to create a
    formatter-object-lookup-table (self._formatters)

        log_level_formats = { <logging-level-A>:<formatting-string>,
                              <logging-level-B>:<formatting-string>,
                              ...
                            }
        self._formatters = { <logging-level-A>:<Formatter-Instance>,
                              <logging-level-B>:<Formatter-Instance>,
                              ...
                            }
    """
    def __init__(self, log_level_formats):
        """
        :param dict log_level_formats: { loglevel : logformat }
        """
        super(HandlerFormatter, self).__init__()

        self._default_level_format = "%(message)s"

        self._default_formatter = logging.Formatter(self._default_level_format,
                                                    datefmt="%m/%d/%Y-%I:%M:%S %p",
                                                   )
        self._formatters = {}
        # Create formatters for each  LEVEL
        if log_level_formats:
            for log_level in log_level_formats:
                self._formatters[log_level] = logging.Formatter(log_level_formats[log_level],
                                                                datefmt="%m/%d/%y-%H:%M",
                                                               )

    @property
    def default_format(self):
        """Default log format to be used for a log level. e.g. "[%(asctime)s - %(message)s]"""
        return self._default_level_format

    @default_format.setter
    def default_format(self, value):
        """SETTER"""
        self._default_level_format = value
        self._default_formatter = logging.Formatter(self._default_level_format)

    def format(self, record):
        """
        Overrides base-class method.
        From base-class docstring:
            Format the specified record as text.

            The record's attribute dictionary is used as the operand to a
            string formatting operation which yields the returned string.

        THIS method passes the record to a Formatter instance, selected by log-level

        :param class record:
        :return: Returns a new instance of the Formatter class.
        :rtype class:
        """
        levelnumber = record.levelno

        formatter = self._formatters.get(levelnumber, self._default_formatter)
        return formatter.format(record)



def log_error(logger):
    """
    Catches and logs exceptions as 'Error'
    """
    def decorated(func_to_wrap):
        """The decorator"""
        @functools.wraps(func_to_wrap)
        def wrapped(*args, **kwargs):
            try:
                return func_to_wrap(*args, **kwargs)
            except Exception as msg:
                # 'logger.exception' always includes 'exc_info' in the log-record.
                logger.exception(msg)
                raise
        return wrapped
    return decorated
