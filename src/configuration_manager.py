"""Handles Application Configuration

    * Provides access to the applications preferences
    (serdes_demo_configuration.yml)

    * Creates and provides access to the plugin-registry.

"""
import os
import re
import sys
import yaml

from facility.logger_configurator import SerdesDemoGetLogger

LOG = SerdesDemoGetLogger().get_logger(__name__)
LOG.debug("Logger Created: %s", LOG.name)


class ConfigurationManager(object):
    """

    Handles plugin registration
    """
    def __init__(self):
        super(ConfigurationManager, self).__init__()
        self._prefs = _get_preferences()
        self._registry = {}
        """Plugin Registry."""

    @property
    def serdes_registry(self):
        """
        This will be a dictionary with items that look like:
         <serdes-format>: [{<serdes-class-name>: <serdes-class>}]

        """
        if not self._registry:
            self._register_serdes()
        return self._registry

    @staticmethod
    def _get_plugin_directory(directory_name):
        """
        :return: str path to location of plugin modules
        """
        module_path = os.path.dirname(__file__)
        return os.path.join(module_path, directory_name)

    def _register_serdes(self):
        """
        Controls the process of creating _registry
        :return:
        """
        # LOAD/IMPORT
        directory_name = self._prefs.get("serdes_directory_name")
        _import_modules_from(directory=self._get_plugin_directory(directory_name),
                             re_pattern=self._prefs.get("serdes_modulename_regex") + ".py")
        # REGISTER
        serdes_data = self._prefs.get("serdes")
        self._add_to_registry(serdes_data)

    def _add_to_registry(self, serdes_data):
        """
        Adds the serdes_data to the registry.
        serdes_data looks like:
            [{'class': 'PickleSerdes', 'module': 'pickle_serdes'},
             {'class': 'JsonSerdes', 'module': 'json_serdes'},
             {'class': 'YamlSerdes', 'module': 'yaml_serdes'}
             ]

        registry will look like:
              {'json': <class 'json_serdes.JsonSerdes'>,
               'pickle': <class 'pickle_serdes.PickleSerdes'>,
               'yaml': <class 'yaml_serdes.YamlSerdes'>
               }
        :param serdes_data:
        :return:
        """
        for serdes_dict in serdes_data:
            serdes_class = _get_loaded_class(serdes_dict["module"], serdes_dict["class"])
            self._registry.update({serdes_dict["format"]: serdes_class})


def _import_modules_from(directory=None, re_pattern=None):
    """
    Import the modules that define serdes.

    This method EXPECTS the naming of serdes-modules and serdes-classes follows the pattern:
            "<format>_serdes.py", "<Format>Serdes"
            e.g. "json_serdes.py", "JsonSerdes"

    :param string serdes_directory:
    """
    if directory not in sys.path:
        sys.path.insert(0, directory)

    # iterate over the py files in path, and IMPORT them
    for _, _, files in os.walk(directory):
        for file_name in files:
            module_name, _ = os.path.splitext(file_name)
            if re.search(re_pattern, file_name):
                map(__import__, [module_name])


def _get_preferences():
    with open(os.environ["PACKAGE_CONFIG"]) as config_file:
        data = config_file.read()
    return yaml.load(data, Loader=yaml.Loader)


def _get_loaded_class(module_name, class_name):
    """
    Return a handle to a loaded class.  The loaded class is found using
    a given a class_name and its module name.
       * Find loaded module named 'module_name'
       * Get loaded class named 'class_name'

    :param string module_name: Name of the module for searching a class.
    :param string class_name: Name of the class to search for.
    :return: handle to loaded class.
    :rtype class:
    """
    try:
        cls = getattr(sys.modules[module_name], class_name)
    except (AttributeError, RuntimeWarning, KeyError):
        LOG.error("Loaded class not found for class_name : %s", class_name)
        return None
    return cls
