"""Entry point for SERDES demo

Get registry for plugin-components
Manage the build process (assemble the runtime components)
Launch use-case execution
"""
# STANDARD IMPORTS
import logging

# LOCAL IMPORTS
from facility.logger_configurator import LoggerConfigurator
from facility.logger_configurator import SerdesDemoGetLogger
ASSEMBLER = False


def launch(cli_args):
    """ SERializer-DESerializer Demo Access Point """
    global ASSEMBLER

    # --------------------
    # Logger-Configuration
    #
    # add default handler to root logger
    logging.basicConfig(level=logging.NOTSET)
    serdes_task_log = logging.getLogger("Serdes_Task_Demo")

    log_configurator = LoggerConfigurator(serdes_task_log,
                                          debug=cli_args.debug,
                                          quiet=cli_args.quiet,
                                         )
    log_configurator.configure_logger()

    # --------------------
    # Imports
    #
    #    These import need to follow Logger-Configuration, as part of the logger-bootstrapping.
    #    The loggers created in the following module imports are setup to
    #    propagate their log-records to the root logger (the only logger with handlers).
    #        If moved to top of file:
    #          * we get error messages, "No handlers found for..."
    #          * the log-records from these modules will go unhandled.
    from serdes_demo import SerdesDemoManager
    from assembler import UseCaseAssembler
    from configuration_manager import ConfigurationManager

    # this kicks-off the plugin-registration process, and gets the results (registry)
    serdes_registry = ConfigurationManager().serdes_registry

    # --------------------
    # Handle Special Case: "formats"-Command-Line Arg
    #
    # short circuit - if a report of supported serial-formats is requested
    if cli_args.formats:
        _report_available_serdes_formats(serdes_registry)
        return
    elif not cli_args.format:
        return

    # --------------------
    # Assemble and Execute the Use-Case Controller
    #
    if not ASSEMBLER:
        ASSEMBLER = True

        # get the serdes for the format specified on the command-line
        serdes = serdes_registry[cli_args.format]

        assembler = UseCaseAssembler(SerdesDemoManager, serdes, cli_args)
        use_case_controller = assembler.get_use_case()
        use_case_controller.execute()


def _report_available_serdes_formats(serdes_registry):
    # create formats list string
    formats = ""
    for data_format in serdes_registry.keys():
        formats += "\t" + data_format + "\n"

    report = "\n\n"
    report += "This SERDES demo (serializer-deserializer)\n"
    report += "supports the following serialization formats:\n\n"
    report += formats
    report += "\nThese are also the FORMAT strings to specify on the command-line \n"
    report += "to run the demo in a specific serialization format. For example:\n"
    report += "\n"
    report += "\t> ./serdes_demo -f yaml"


    print report
