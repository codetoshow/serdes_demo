"""Serializer-Deserializer [SERDES] for PICKLE"""
# STANDARD IMPORTS
try:
    import cPickle as pickle
except:
    import pickle


# LOCAL IMPORTS
from serdes_interface import SerdesInterface



class PickleSerdes(SerdesInterface):
    """

    """
    def __init__(self):
        super(PickleSerdes, self).__init__()

    def read(self, data):
        return pickle.loads(data)

    def write(self,  data):
        return pickle.dumps(data)

