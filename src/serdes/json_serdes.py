"""Serializer-Deserializer [SERDES] for JSON"""

# STANDARD IMPORTS
import json


# LOCAL IMPORTS
from serdes_interface import SerdesInterface


class JsonSerdes(SerdesInterface):
    """

    """
    def __init__(self):
        super(JsonSerdes, self).__init__()

    def read(self, data):
        return json.loads(data)

    def write(self, data):
        return json.dumps(data)
