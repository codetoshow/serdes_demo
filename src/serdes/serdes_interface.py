"""Serializer-Deserializer [SERDES] interface"""


class SerdesInterface(object):
    """SERDES implement this interface
       Use-case is coded to use this interface
    """
    def read(self, data):
        raise NotImplementedError

    def write(self, data):
        raise NotImplementedError


