"""Serializer-Deserializer [SERDES] for YAML"""
# STANDARD IMPORTS
import yaml
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


# LOCAL IMPORTS
from serdes_interface import SerdesInterface


class YamlSerdes(SerdesInterface):
    """

    """
    def __init__(self):
        super(YamlSerdes, self).__init__()

    def read(self, data):
        return yaml.load(data, Loader=Loader)

    def write(self, data):
        return yaml.dump(data, Dumper=Dumper)
