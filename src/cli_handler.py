"""Command-Line Interface Handler"""
# STANDARD IMPORTS
import argparse
import sys


def get_command_line_arguments():
    """
    Parse and return the command line arguments
    """
    f_help = "Specify serialization format to use"
    l_help = "List available data serialization formats"
    d_help = "Logging Control:Generate debug unwrap_log messages."
    q_help = "Logging Control: Silence info unwrap_log messages."
    m_help = "Use a larger test data-set"

    parser = argparse.ArgumentParser()

    format_group = parser.add_mutually_exclusive_group()
    format_group.add_argument("-l", "--formats", action="store_true", help=l_help)
    format_group.add_argument("-f", "--format", type=str, help=f_help)
    log_level_group = parser.add_mutually_exclusive_group()
    log_level_group.add_argument("-d", "--debug", action="store_true", help=d_help)
    log_level_group.add_argument("-q", "--quiet", action="store_true", help=q_help)
    parser.add_argument("-D", "--more_data", action="store_true", help=m_help)

    # Handle no command-line args specified
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()

    return args
