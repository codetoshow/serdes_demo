"""
Assembler - assembles the runtime configuration
"""
import os


from facility.logger_configurator import SerdesDemoGetLogger

LOG = SerdesDemoGetLogger().get_logger(__name__)
LOG.debug("Logger Created: {}".format(LOG.name))



class UseCaseAssembler(object):
    """
    Assembles the Command-Class
      * using constructor dependency injection
      * usage example:
          assembler = UseCaseAssembler(SerdesDemoManager, serdes, cli_args)
          use_case_controller = assembler.get_use_case()
          use_case_controller.execute()

    :param use_case_class:
    :param serdes: the SERDES of the requested format (strategy)
    :param cli_args: command-line arguments dictionary
    """
    def __init__(self, use_case_class, serdes, cli_args):
        LOG = SerdesDemoGetLogger().get_logger(__name__)
        LOG.debug("UseCaseAssembler.__init__()")
        self._use_case_cls = use_case_class
        self._use_case = None
        self._cli_args = cli_args
        self._test_data_file = None
        self._serdes = serdes()
        LOG.debug("serdes: %s", serdes)

    def _set_test_data_file(self, more_data=False):
        """"""
        if more_data:
            data_file = "test_data.json"
        else:
            data_file = "test_data_small.json"

        tdf = os.path.join(os.environ["PACKAGE_ROOT"], "data", data_file)
        LOG.debug("Test-File: %s", tdf)
        self._test_data_file = tdf

    def _assemble_use_case(self):
        """
        Performs the use-case assembly using a use-case-specific builder
        ... calls all the setter methods of the concrete builder
        """
        LOG.debug("\tASSEMBLING USE-CASE")
        self._set_test_data_file(self._cli_args.more_data)
        self._use_case = self._use_case_cls(serdes=self._serdes,
                                            test_data_file=self._test_data_file)
        LOG.debug("\tASSEMBLING USE-CASE............DONE")

    def get_use_case(self):
        """Assembles and returns the UC-Controller"""
        self._assemble_use_case()
        return self._use_case
