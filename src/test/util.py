import json
import os
import re
import sys
import yaml


def get_test_data_object():
    sep = "%s" % os.path.sep

    this_dir = os.path.dirname(os.path.abspath(__file__))
    this_dir_parts = this_dir.split(sep)
    data_dir_parts = this_dir_parts[:-2]
    data_dir_parts.extend(["data", "test_data_small.json"])
    data_file = sep.join(data_dir_parts)
    with open(data_file) as df:
        data = df.read()
    return json.loads(data)


def get_preferences(prefs_file):
    with open(prefs_file) as f:
        data = f.read()
    return yaml.load(data, Loader=yaml.Loader)


def get_plugin_directory(directory_name):
    """
    :return: str path to location of plugin modules
    """
    module_path = os.path.dirname(__file__)
    return os.path.join(source_root, directory_name)


def get_src_root():
    bin_dir = os.path.dirname(os.path.abspath(__file__))
    bin_dir_parts = bin_dir.split(os.path.sep)
    path_parts = bin_dir_parts[:-1]
    src_root = os.path.join('/', *path_parts)
    path_parts = bin_dir_parts[:-2]
    pkg_root = os.path.join('/', *path_parts)
    return pkg_root, src_root


def import_modules_from(directory=None, re_pattern=None):
    """
    Import the modules that define serdes.

    This method EXPECTS the naming of serdes-modules and serdes-classes follows the pattern:
            "<format>_serdes.py", "<Format>Serdes"
            e.g. "json_serdes.py", "JsonSerdes"

    :param string serdes_directory:
    """
    if directory not in sys.path:
        sys.path.insert(0, directory)

    # iterate over the py files in path, and IMPORT them
    for _, _, files in os.walk(directory):
        for file_name in files:
            module_name, file_ext = os.path.splitext(file_name)
            if re.search(re_pattern, file_name):
                map(__import__, [module_name])


pkg_root, source_root = get_src_root()
sys.path.append(source_root)

prefs = get_preferences(pkg_root + "/etc/serdes_demo_configuration.yml")

serdes_dicts = prefs.get("serdes")
import_modules_from(directory=get_plugin_directory("serdes"),
                    re_pattern="_serdes")
