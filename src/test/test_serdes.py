"""Test SERDES plugins"""

import sys

from util import get_test_data_object
from util import serdes_dicts


def test_serdes():
    test_object = get_test_data_object()

    for serdes_info in serdes_dicts:
        cls = serdes_info.get("class")
        module = serdes_info.get("module")
        cls = getattr(sys.modules[module], cls)
        serdes = cls()

        serialized = serdes.write(test_object)
        deserialized = serdes.read(serialized)

        assert test_object == deserialized
        assert test_object is not deserialized
