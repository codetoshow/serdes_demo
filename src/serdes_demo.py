"""Use-Case

Demo data serialization, deserialization

        * Input data (simulated Read serialized data from file)
        * Serialize data
        * Deserialize data
        * Display results
"""
import json
from facility.logger_configurator import SerdesDemoGetLogger

LOG = SerdesDemoGetLogger().get_logger(__name__)
LOG.debug("Logger Created: %s", LOG.name)


class SerdesDemoManager(object):
    """Use-Case Manager
    (Implements the Command-Pattern)
    """
    def __init__(self, serdes=None, test_data_file=None):
        LOG.debug("SerdesDemoManager.__init__()  <--- use case")

        self.serdes = serdes
        self.test_data_file = test_data_file
        """set via setter DI (setters in builder/configurator)"""

        self._input_data_object = ""
        self._serialized_data_object = ""
        self._deserialized_data = ""

    def execute(self):
        """Command-class execute"""
        LOG.debug("USE-CASE  EXECUTE ")
        self._set_input_data_object()
        self._set_serialize_data_object(self._input_data_object)
        self._set_deserialize_data(self._serialized_data_object)
        self._display_results()

    def _set_input_data_object(self):
        """
        return: data-object; deserialized data
        """
        with open(self.test_data_file) as data_file:
            ser_data = data_file.read()

        self._input_data_object = json.loads(ser_data)

    def _set_serialize_data_object(self, data_object):
        self._serialized_data_object = self.serdes.write(data_object)

    def _set_deserialize_data(self, serialized_data_object):
        self._deserialized_data = self.serdes.read(serialized_data_object)

    def _deserialized_data_is_equivalent_to_test_data(self):
        if self._deserialized_data == self._input_data_object:
            return True
        return False

    def _deserialized_data_is_not_test_data(self):
        if self._deserialized_data is not self._input_data_object:
            return True
        return False

    def _display_results(self):
        """assemble results string
           send to display
        """
        verification0 = self._deserialized_data_is_equivalent_to_test_data()
        verification1 = self._deserialized_data_is_not_test_data()

        msg = "\n\n"
        msg += "\t\tSERDES RESULTS\n"
        msg += "\t\t==============\n\n\n"
        msg += "'Input' Test Data:\n\n {}\n\n\n".format(self._input_data_object)
        msg += "Serialized Data:\n\n {}\n\n".format(self._serialized_data_object)
        msg += "Deserialized Data:\n\n {}\n\n\n".format(self._deserialized_data)
        msg += "Deserialized Data == Test Data: {}\n".format(verification0)
        msg += "Deserialized Data is not Test Data: {}\n\n".format(verification1)

        print msg
